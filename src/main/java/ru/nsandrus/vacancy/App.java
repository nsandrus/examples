package ru.nsandrus.vacancy;

import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

// запуск несколько потоков
class Runner extends Thread {

	App app;

	Runner(App app) {
		this.app = app;
	}

	@Override
	public void run() {
		int key = new Random().nextInt(4);
		System.out.println(" " + Thread.currentThread().getName() + " key=" + key + " value=" + app.compute(key));
	}
}

public class App {
	private final ConcurrentHashMap<Integer, Integer> cache = new ConcurrentHashMap<Integer, Integer>();

	public Integer compute(Integer key) {

		return cache.computeIfAbsent(key, k -> {
			System.out.println("key=" + k);
			try {
				Thread.sleep(200);
			} catch (Exception e) {
			}
			return k * 10;
		});

	}

	public static void main(String[] args) {
		App app = new App();

		for (int i = 0; i < 10; i++) {
			new Runner(app).start();
		}

	}
}
